package com.api.proyecto.controller;

import com.api.proyecto.dto.Mensaje;
import com.api.proyecto.dto.ObraDto;
import com.api.proyecto.model.Obra;
import com.api.proyecto.service.ObraService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/obra")
public class ObraController {

    @Autowired
    ObraService obraService;

    @GetMapping("/lista")
    public ResponseEntity<List<Obra>> list(){
        List<Obra> list = obraService.List();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    //busqueda por id
     @GetMapping("/detail/{id}")
    public ResponseEntity<Obra> getById(@PathVariable("id") int id){
        if(!obraService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Obra obra = obraService.getOne(id).get();
        return  new ResponseEntity(obra, HttpStatus.OK);
    }

    //busqueda por nombre
    @GetMapping("/detailname/nombreObra")
    public ResponseEntity<Obra> getByNombreObra(@PathVariable("nombreObra") String nombreObra){
        if(!obraService.existsByNombreObra(nombreObra))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Obra obra = obraService.getByNombreObra(nombreObra).get();
        return  new ResponseEntity(obra, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<?>create(@RequestBody ObraDto obraDto){
        if (StringUtils.isBlank(obraDto.getNombreObra()))
            return new ResponseEntity<>(new Mensaje("el nombre es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getFechaInicio()))
            return new ResponseEntity<>(new Mensaje("la fecha inicial es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getFechaFinal()))
            return new ResponseEntity<>(new Mensaje("la fecha final es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getUbicacion()))
            return new ResponseEntity<>(new Mensaje("la ubicacion de la obra es obligatoria"),HttpStatus.BAD_REQUEST);
        Obra obra = new Obra(obraDto.getNombreObra(), obraDto.getFechaInicio(), obraDto.getFechaFinal(), obraDto.getUbicacion());
        obraService.save(obra);
        return new ResponseEntity<>(new Mensaje("obra creada"),HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?>update(@PathVariable("id") int id, @RequestBody ObraDto obraDto){
        if(!obraService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        // valida de que el nombre de la obra no sea igual al nombre de otra obra
        if (obraService.existsByNombreObra(obraDto.getNombreObra()) && obraService.getByNombreObra(obraDto.getNombreObra()).get().getId() != id)
            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getNombreObra()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getFechaInicio()))
            return new ResponseEntity(new Mensaje("la fecha inicial es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getFechaFinal()))
            return new ResponseEntity(new Mensaje("la fecha final es obligatorio"),HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(obraDto.getUbicacion()))
            return new ResponseEntity(new Mensaje("la ubicacion de la obra es obligatoria"),HttpStatus.BAD_REQUEST);
        Obra obra =  obraService.getOne(id).get();
        obra.setNombreObra(obraDto.getNombreObra());
        obra.setFechaInicio(obraDto.getFechaInicio());
        obra.setFecha_Final(obraDto.getFechaFinal());
        obra.setUbicacion(obraDto.getUbicacion());
        obraService.save(obra);
        return new ResponseEntity<>(new Mensaje("obra actualizada"),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete (@PathVariable("id")int id){
        if(!obraService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        obraService.delete(id);
        return new ResponseEntity<>(new Mensaje("obra eliminada"),HttpStatus.OK);
    }


}
