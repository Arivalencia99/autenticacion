package com.api.proyecto.repository;

import com.api.proyecto.model.Obra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ObraRepository  extends JpaRepository<Obra, Integer> {
    //buscar por nombre
    Optional<Obra> findByNombreObra(String nombreObra);
    //existe por nombre
    boolean existsByNombreObra(String nombreObra);

}
