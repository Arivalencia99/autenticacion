package com.api.proyecto.security.enums;

public enum RolNombre {
    ROLE_ADMIN, ROLE_USER
}
