package com.api.proyecto.dto;

import com.sun.istack.NotNull;

public class ObraDto {

    @NotNull
    private  String nombreObra;
    @NotNull
    private String fechaInicio;
    @NotNull
    private String fechaFinal;
    @NotNull
    private String ubicacion;

    public ObraDto() {
    }

    public ObraDto(String nombreObra, String fechaInicio, String fechaFinal, String ubicacion) {
        this.nombreObra = nombreObra;
        this.fechaInicio = fechaInicio;
        this.fechaFinal = fechaFinal;
        this.ubicacion = ubicacion;
    }

    public String getNombreObra() {
        return nombreObra;
    }

    public void setNombreObra(String nombreObra) {
        this.nombreObra = nombreObra;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fecha_Final) {
        this.fechaFinal = fecha_Final;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
